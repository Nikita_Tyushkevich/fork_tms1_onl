print("Выберите операцию: 1. Сложение 2. Вычитание 3. Умножение 4. Деление ")

input_1 = input("Введите номер пункта меню: ")
first_num = int(input("Введите первое число: "))
second_num = int(input("Введите второе число: "))


def addition(a, b):
    result_add = first_num + second_num
    return result_add


def subtraction(a, b):
    result_sub = first_num - second_num
    return result_sub


def multiplication(a, b):
    result_mul = first_num * second_num
    return result_mul


def division(a, b):
    try:
        result_div = first_num / second_num
        return result_div
    except ZeroDivisionError:
        return "Не надо так"


def calc(input_1):
    if input_1 == "1":
        print(addition(first_num, second_num))
    elif input_1 == "2":
        print(subtraction(first_num, second_num))
    elif input_1 == "3":
        print(multiplication(first_num, second_num))
    elif input_1 == "4":
        print(division(first_num, second_num))
    else:
        print("Выбрана неверная функция.")


calc(input_1)
