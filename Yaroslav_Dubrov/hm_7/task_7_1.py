def reshape(input_list: list, rows_num: int, columns_num: int):

    # Check if parameters are correct
    # rows_num * columns_num should be eq len(input_list) to create matrix
    if rows_num * columns_num == len(input_list):

        # print splitted lists one by one rows_num times
        for i in range(rows_num):
            print(input_list[i::rows_num])
    else:
        print("Wrong parameters")


input_list = [6, 4, 3, 4, 5, 6]
rows_num = 2
columns_num = 3


reshape(input_list, rows_num, columns_num)
