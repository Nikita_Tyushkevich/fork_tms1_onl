text = 'Meet my family. There are five of us - my parents, my elder brother.'
text_low = text.lower()
a = list(text_low)
b = set()
for i in a:
    if i == '.' or i == ',' or i == ' ' or i == '-' or i == '!' or i == '?':
        continue
    else:
        b.add(i)


most_common = None
qty_most_common = 0

for item in b:
    qty = a.count(item)
    if qty > qty_most_common:
        qty_most_common = qty
        most_common = item
print(f'Самая частая буква это: {most_common} '
      f'и повторяется она {qty_most_common} раз')
