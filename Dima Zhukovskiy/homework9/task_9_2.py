class Investment:

    def __init__(self, N, R):
        self.N = N
        self.R = R
        self.Month = 12 * self.R


class Bank:

    def deposit(self, n, month):
        if month == 0:
            return n
        return self.deposit(n, month - 1) * (1 + 10 / 100 / 12)


bank = Bank()
vklad = Investment(14, 10)
print(bank.deposit(vklad.N, vklad.Month))

# Проверля тут https://calcus.ru/kalkulyator-vkladov
