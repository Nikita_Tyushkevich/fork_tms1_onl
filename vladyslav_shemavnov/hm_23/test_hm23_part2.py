from selenium import webdriver
from selenium.webdriver.common.by import By
import pytest
import time


@pytest.fixture(scope='function')
def setup():
    browser = webdriver.Chrome()
    browser.implicitly_wait(5)
    browser.get("https://ultimateqa.com/filling-out-forms/")
    yield browser
    browser.quit()


def test_success_check_form(setup):
    page = setup

    time.sleep(2)
    name_field = page.find_element(By.ID, "et_pb_contact_name_0")
    name_field.send_keys("Vlados")

    time.sleep(2)
    message_field = page.find_element(By.ID, "et_pb_contact_message_0")
    message_field.send_keys("Hello World Hello World Hello World Hello World")

    button = page.find_element(By.XPATH, "//form[1]//button")
    button.click()
    xpath = "//div[@class='et-pb-contact-message']" \
            "//p[contains(., 'Thanks for contacting us')]"

    assert page.find_element(By.XPATH, xpath).is_displayed()


def test_dont_fill_name(setup):
    page = setup
    time.sleep(2)
    message_field = page.find_element(By.ID, "et_pb_contact_message_0")
    message_field.send_keys("Hello World Hello World "
                            "Hello World Hello World")

    button = page.find_element(By.XPATH, "//form[1]//button")
    button.click()
    xpath = "//div[@id='et_pb_contact_form_0']" \
            "//div//ul/li[contains(., 'Name')]"

    assert page.find_element(By.XPATH, xpath).is_displayed()


def test_dont_fill_message(setup):
    page = setup
    time.sleep(2)
    name_field = page.find_element(By.ID, "et_pb_contact_name_0")
    name_field.send_keys("Vlados")

    button = page.find_element(By.XPATH, "//form[1]//button")
    button.click()

    xpath = "//div[@id='et_pb_contact_form_0']" \
            "//div//ul/li[contains(., 'Message')]"

    assert page.find_element(By.XPATH, xpath).is_displayed()
