a = {'a': 1, 'b': 2, 'c': 3}
b = {'c': 3, 'd': 4, 'e': 5}

ab = {**a, **b}
# это пока костыль, ещё думаю над решением
for i in ab.keys():
    if i in a and i in b:
        ab[i] = [a[i], b[i]]
    else:
        ab[i] = [None, ab[i]]

print(ab)
