def validate(card_number):

    def digits_of(n):
        return [int(i) for i in str(n)]

    digits = digits_of(card_number)
    odd_digits = digits[-1::-2]
    even_digits = digits[-2::-2]
    checksum = 0
    checksum += sum(odd_digits)
    for i in even_digits:
        checksum += sum(digits_of(i * 2))
    return checksum % 10 == 0


print(validate("378282246310005"))
print(validate("371449635398431"))
print(validate("378734493671000"))
print(validate("5610591081018250"))
print(validate("30569309025904"))
print(validate("38520000023237"))
print(validate("6011111111111117"))
print(validate("6011000990139424"))
print(validate("3530111333300000"))
print(validate("3566002020360505"))
print(validate("5555555555554444"))
print(validate("5105105105105100"))
print(validate("4111111111111111"))
print(validate("4012888888881881"))
print(validate("4222222222222"))
print(validate("76009244561"))
print(validate("5019717010103742"))
print(validate("6331101999990016"))
