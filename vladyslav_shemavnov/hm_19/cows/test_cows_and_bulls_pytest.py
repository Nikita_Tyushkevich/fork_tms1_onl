import pytest
from lesson5 import algorithm_for_game


@pytest.fixture(scope="function")
def my_fixture():
    print("I am your fixture")


class TestMyClass(object):

    @pytest.mark.parametrize("number_for_game, correct_answer",
                             [("3245", "3254"), ("4568", "5468")])
    def test_success(self, number_for_game, correct_answer, my_fixture):
        assert algorithm_for_game(number_for_game,
                                  correct_answer), [2, 2]

    @pytest.mark.xfail(reason="using wrong data")
    @pytest.mark.parametrize("number_for_game, correct_answer",
                             [("3245", "3264"), ("4568", "5400")])
    def test_failed(self, number_for_game, correct_answer, my_fixture):
        assert algorithm_for_game(number_for_game,
                                  correct_answer), [2, 2]
