sentence = ("thequick brown fox jumps over the lazy dog")


def numbers(sent):
    for el in sent.split(" "):
        if el != "the":
            yield len(el)


numbers(sentence)
print([i for i in numbers(sentence)])
