from selenium import webdriver


def test():
    url = "http://the-internet.herokuapp.com/frames"
    path = "C:/chromedriver.exe"
    driver = webdriver.Chrome(path)
    driver.implicitly_wait(5)
    driver.get(url)
    link = driver.find_element_by_xpath('//*[@id="content"]/div/ul/li[2]/a')
    link.click()
    driver.switch_to.frame(driver.find_element_by_xpath(
        '//*[@id="mce_0_ifr"]'))
    iframe = driver.find_element_by_xpath('//*[@id="tinymce"]/p')
    iframe.click()
    result = driver.find_element_by_xpath('//*[@id="tinymce"]/p').text
    assert "Your content goes here." == result
    driver.quit()
