from main_bulls_cows import bulls_cows
import pytest
import time


@pytest.fixture(autouse=True)
def start_bulls_cows_test():
    print("launching test...")
    time.sleep(1)
    print("Test launched...")


@pytest.mark.positive
def test_bulls_4():
    assert bulls_cows(4569, 4569) == [4, 0]


@pytest.mark.positive
def test_bulls_3():
    assert bulls_cows(4569, 4568) == [3, 0]


@pytest.mark.positive
def test_bulls_2():
    assert bulls_cows(4569, 4500) == [2, 0]


@pytest.mark.positive
def test_bulls_1():
    assert bulls_cows(4569, 4000) == [1, 0]


@pytest.mark.positive
def test_cows_4():
    assert bulls_cows(4569, 9654) == [0, 4]


@pytest.mark.positive
def test_cows_3():
    assert bulls_cows(4569, 9054) == [0, 3]


@pytest.mark.positive
def test_cows_2():
    assert bulls_cows(4569, 9004) == [0, 2]


@pytest.mark.positive
def test_cows_1():
    assert bulls_cows(4569, 3004) == [0, 1]


@pytest.mark.xfail
def test_less_then_four_symbols():
    assert bulls_cows(4569, 4)


@pytest.mark.xfail
def test_with_not_int():
    assert bulls_cows(4569, "abc")


@pytest.mark.xfail
def test_empty():
    assert bulls_cows(4569, "")
