from abc import ABC, abstractmethod


class Methods(ABC):
    @abstractmethod
    def add(self, *args):
        raise NotImplementedError

    @abstractmethod
    def subtract(self, *args):
        raise NotImplementedError

    @abstractmethod
    def multiply(self, *args):
        raise NotImplementedError

    @abstractmethod
    def divide(self, *args):
        raise NotImplementedError

    @abstractmethod
    def type_validator(self, *args):
        raise NotImplementedError


class Calculator(Methods):

    def add(self, args1, args2):
        if self.type_validator(args1, args2):
            print(args1 + args2)

    def subtract(self, args1, args2):
        if self.type_validator(args1, args2):
            print(args1 - args2)

    def multiply(self, args1, args2):
        if self.type_validator(args1, args2):
            print(args1 * args2)

    def divide(self, args1, args2):
        if self.type_validator(args1, args2):
            if args2 == 0:
                result = "Impossible to divide by zero"
            else:
                result = args1 / args2
            print(result)

    def type_validator(self, args1, args2):
        if not isinstance(args1, (int, float)) \
                and not isinstance(args2, (int, float)):
            print("Type number should be int or float")
            return False
        else:
            return True


Calculator()
