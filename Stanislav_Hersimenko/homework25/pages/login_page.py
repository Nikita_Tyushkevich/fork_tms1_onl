from pages.base_page import BasePage
from locators.login_page_locators import LoginPageLocator


class LoginPage(BasePage):
    def should_be_login_page(self):
        assert_text = "CREATE AN ACCOUNT"
        create_account_text = self.find_element(
            LoginPageLocator.LOCATOR_CREATE_ACCOUNT_TEXT).text
        assert create_account_text == assert_text, \
            f'Text on the page {create_account_text} does not eq{assert_text}'

    def login(self, email: str, passwd: str):
        email_field = self.find_element(
            LoginPageLocator.LOCATOR_EMAIL_LOGIN_FIELD)
        email_field.send_keys(email)
        passwd_field = self.find_element(LoginPageLocator.LOCATOR_PASSWD_FIELD)
        passwd_field.send_keys(passwd)
        self.find_element(LoginPageLocator.LOCATOR_SUBMIT_BUTTON).click()
