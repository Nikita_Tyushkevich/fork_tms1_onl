from selenium.webdriver.common.by import By


class CartPageLocators:

    LOCATOR_CART_TITLE = (By.XPATH, '//*[@id="columns"]/div[1]/span[2]')
    LOCATOR_CART_SUMMARY = (By.ID, "cart_title")
    LOCATOR_CART_EMPTY = (By.XPATH, '//*[@id="center_column"]/p')
    LOCATOR_CART_NOT_EMPTY = (By.XPATH, '//*[@id="cart_title"]/span')
    LOCATOR_CART = (
        By.XPATH, '//*[@id="header"]/div[3]/div/div/div[3]/div/a')
    LOCATOR_ITEM_DELETE = (By.CLASS_NAME, 'icon-trash')
