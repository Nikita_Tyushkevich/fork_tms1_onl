import re


def validate_card(card):
    """Преобразовываем для тестов"""
    converting = str(card)
    """Валидиция только чисел"""
    result = bool(re.findall(r"\D", converting))
    if result:
        return print("Только цифры! ")

    card_numbers = list(map(int, converting[::-1]))  # Переворачиваем список
    """Создаем список только с четными числами из списка"""
    uneven_number_list = card_numbers[0::2]  # Не четные
    even_number_list = card_numbers[1::2]  # Только четные
    list_result = []
    for i in even_number_list:  # Высчитываем
        var = i * 2
        if var > 9:
            c = (''.join(str(var)))
            g = sum(list(map(int, c)))
            list_result.append(g)
        else:
            list_result.append(var)

    sum_result = sum(uneven_number_list + list_result)  # Высчитываем резалт
    if sum_result % 10 == 0:
        return print('True')
    else:
        return print('False')


# Test
validate_card(4561261212345464)
validate_card(4561261212345467)
validate_card(371449635398431)
validate_card('фыафы')
validate_card('----')
