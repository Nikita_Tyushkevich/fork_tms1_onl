import re


def dec_calculator(func):
    def wrapper(us_input):
        # Получаем списки с числами и операторами
        numbers = list(map(int, re.findall(r"\d+", us_input)))
        operators = re.findall(r"[^!@#$%^&()№;:?0-9]+", us_input)

        # Валидация букв
        if re.findall(r'[aA-zZ-аА-яЯ-ёЁ]', us_input):
            print('Буквы не должны присутствовать')
        result = []

        for it in operators:
            if it == '*':
                a = operators.index('*')
                arr_re = numbers[a] * numbers[a + 1]
                result = arr_re + numbers[0]
            if it == '/':
                a = operators.index('/')
                arr_re = numbers[a] // numbers[a + 1]
                result = arr_re + numbers[0]
            if it == '+':
                a = operators.index('+')
                arr_re = numbers[a] + numbers[a + 1]
                result = arr_re + numbers[0]
            if it == '-':
                a = operators.index('-')
                arr_re = numbers[a] - numbers[a + 1]
                result = arr_re + numbers[0]
        func(print(result))

    return wrapper


@dec_calculator
def fun_calculator(user_inpt):
    return user_inpt


print("Введите математическое выражение ниже")
"""Test 2+3*4"""
user_input = input("~ ")
fun_calculator(user_input)
