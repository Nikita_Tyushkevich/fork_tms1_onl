def likes(*users):
    lst_name = list(users)
    counter = len(users)  # Счетчик юзеров

    text_result = 'like this'  # Вынес отделно последние слова
    if counter == 0:  # Если один юзер
        print(f'no one {text_result}')
    elif counter == 1:  # Если один юзер
        user_1 = lst_name[0]
        print(f'{user_1} {text_result}')
    elif counter == 2:  # Если два юзера
        two_users = ' and '.join(users)
        print(f'{two_users} {text_result}')
    elif counter == 3:  # Если три юзера костыльно
        user_1 = lst_name[0]
        user_2 = lst_name[1]
        user_3 = lst_name[2]
        print(f'{user_1}, {user_2} and {user_3} {text_result}')
    else:   # Указываем два юзера и вычетаем из остальных их
        user_1 = lst_name[0]
        user_2 = lst_name[1]
        print(f'{user_1}, {user_2} and {counter - 2} others {text_result}')


# Тест
likes()
likes('Anna')
likes('Anna', 'Alex')
likes('Anna', 'Alex', 'Mark')
likes('Anna', 'Alex', 'Mark', 'Max')
