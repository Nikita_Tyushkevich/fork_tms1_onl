from locators import Locators
from selenium import webdriver
import pytest


@pytest.fixture(scope='session', autouse=True)
def browser():
    driver = webdriver.Chrome()
    driver.get('http://demo.guru99.com/test/newtours/register.php')
    driver.implicitly_wait(10)
    yield driver
    driver.quit()


@pytest.mark.task_21_2
def test_demo_guru(browser):
    locat = Locators()
    first_name = 'fname'
    laslt_name = 'lname'
    user_name = 'uname'
    """сделал отдельный метод чтобы не выходить за 79 символов"""
    find_element = browser.find_element_by_css_selector
    find_element(locat.first_name).send_keys(first_name)
    find_element(locat.last_name).send_keys(laslt_name)
    find_element(locat.phone).send_keys('phone')
    find_element(locat.email).send_keys('email')
    find_element(locat.address).send_keys('address')
    find_element(locat.city).send_keys('city')
    find_element(locat.state).send_keys('state')
    find_element(locat.postal_code).send_keys('pc')
    find_element(locat.user_name).send_keys(user_name)
    find_element(locat.password).send_keys('1234qwer')
    find_element(locat.confirm_pass).send_keys('1234qwer')
    find_element(locat.button).click()
    var_1 = browser.find_element_by_xpath(locat.title_last_first_name).text
    var_2 = browser.find_element_by_xpath(locat.user_name_title).text
    list_fname_l_name = var_1.split()
    list_user_name = var_2.split()
    assert first_name, 'l_name,' in list_fname_l_name
    assert 'uname.' == list_user_name[-1]
