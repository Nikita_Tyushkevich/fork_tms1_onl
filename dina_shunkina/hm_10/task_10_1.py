""""
В задании выполняются следующие пункты:
1. Собрать букет
2. Определить стоимости букета
3. Определить время его увядания по среднему времени жизни всех цветов в букете
4. Выполняется сортировка цветов в букете на основе (цвет и стоимость)
5. Выполняется поиск цветов в букете по определенным параметрам
6. Проверка наличия цветка в букете
"""


class Flower:

    def __init__(self, name, color, time, cost):
        self.name = name
        self.color = color
        self.time = time
        self.cost = cost


class Bouquet:
    """1. Собрать букет"""

    def __init__(self):
        self.flowers = []
        self.accessories = []

    def add_flowers(self, *flowers):

        for flower in flowers:
            self.flowers.append(flower)

    def add_accessories(self, *accessories):

        for accessory in accessories:
            self.accessories.append(accessory)

    """2. Определить стоимости букета"""

    def find_cost_bouquet(self):

        cost_bouquet = sum([flower.cost for flower in self.flowers])
        acces_cost = sum([accessory.cost for accessory in self.accessories])
        cost = cost_bouquet + acces_cost
        return f"Стоимость букета: {cost} USD"

    """3.Время увядания букета по среднему времени жизни цветов в букете"""

    def find_time_life(self):

        time_life = sum([flower.time for flower in self.flowers])
        length_time = round(time_life / len(self.flowers))
        return f"Время увядания букета: {length_time} дней"

    """4. Сортировка цветов в букете на основе параметров (цвет и стоимость)"""

    def sort_bouquet(self, parameter):
        bouquets = sorted(self.flowers, key=lambda a: getattr(a, parameter))
        flower_name = [flower.name for flower in bouquets]
        return f"Сортировка цветов по {parameter}: {flower_name}"

    """5. Выполняется поиск цветов в букете по определенным параметрам"""

    def find_flower_parameter(self, parameter, cost):
        result = [flower.name for flower in self.flowers
                  if getattr(flower, parameter) == cost]
        return f"По {parameter} и {cost} найдено: {result}"

    """6. Проверка наличия цветка в букете"""

    def find_flower(self, name):
        flower_name = [flower.name for flower in self.flowers]
        if name in flower_name:
            return f"{name} есть в букете."
        else:
            return f"Цветка {name} нет в букете."


rose = Flower("Роза", "красный", 8, 20)
lily = Flower("Лилия", "белый", 3, 17)
peony = Flower("Пион", "розовый", 6, 15)

bouquet = Bouquet()
bouquet.add_flowers(rose, lily, peony)

print(bouquet.find_cost_bouquet())
print(bouquet.find_time_life())
print(bouquet.sort_bouquet("color"))
print(bouquet.sort_bouquet("cost"))
print(bouquet.find_flower_parameter("cost", 20))
print(bouquet.find_flower("Лилия"))
print(bouquet.find_flower("Фиалка"))
