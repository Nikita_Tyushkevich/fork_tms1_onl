"""Напечатайте по порядку данные о дате заказа,
id продавца, номер заказа, количество"""

import mysql.connector as mysql

db = mysql.connect(
    host="localhost",
    user="root",
    passwd="",
    database="task_26"
)
cursor = db.cursor()
query = "SELECT ord_date, customer_id, ord_no, purch_amt FROM orders"
cursor.execute(query)
print(cursor.fetchall())
