"""
1. программа ожидает от пользователя ввода
математического выражения и правильно его трактует
p.s. каждый элемент вводиться через пробел в матиматическом выражении
"""


def decorator(func):
    def wrapper(operation):
        list_with_numbers = []
        list_with_symbols = []
        operation_list = operation.split()
        for i in operation_list:
            if i != "+" and i != "-" and i != "*" and i != "/":
                list_with_numbers.append(i)  # ["1", "3"]
            elif i == "+" or i != "-" or i != "*" or i != "/":
                list_with_symbols.append(i)  # ["+"]
        for y in list_with_symbols:
            if y == "+":
                answer = int(list_with_numbers[0]) + int(list_with_numbers[1])
                print(answer)
            elif y == "-":
                answer = int(list_with_numbers[0]) - int(list_with_numbers[1])
                print(answer)
            elif y == "*":
                answer = int(list_with_numbers[0]) * int(list_with_numbers[1])
                print(answer)
            elif y == "/":
                answer = int(list_with_numbers[0]) / int(list_with_numbers[1])
                print(answer)
        func(operation)

    return wrapper


@decorator
def my_operation(operation_input):
    return operation_input


str1 = input("Введите математическое выражения согласно условию:")
my_operation(str1)
