from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

browser = webdriver.Chrome("./chromedriver")
browser.implicitly_wait(5)
browser.get("http://the-internet.herokuapp.com/dynamic_controls")

checkbox_button = browser.find_element_by_xpath(
    "//input[@type='checkbox']").click()
remove_button = browser.find_element_by_xpath(
    "//button[text()='Remove']").click()
find_text_1 = WebDriverWait(browser, 10).until(
    EC.presence_of_element_located((By.XPATH, "//p[@id='message']"))
)
check_checkbox = WebDriverWait(browser, 10).until(
    EC.invisibility_of_element_located((
        By.XPATH, "//input[@type='checkbox']"))
)
disabled_field = WebDriverWait(browser, 10).until_not(
    EC.element_to_be_clickable((By.XPATH, "//input[@type='text']"))
)
enable_button = browser.find_element_by_xpath(
    "//button[text()='Enable']").click()
find_text_2 = WebDriverWait(browser, 10).until(
    EC.presence_of_element_located((By.XPATH, "//p[@id='message']"))
).text
enabled_field = WebDriverWait(browser, 10).until(
    EC.element_to_be_clickable((By.XPATH, "//input[@type='text']"))
)

browser.quit()
