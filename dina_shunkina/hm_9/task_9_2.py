class Investment:
    def __init__(self, sum_investment, term_investment, annual_percentage):
        self.sum_investment = sum_investment
        self.term_investment = term_investment
        self.annual_percentage = annual_percentage


class Bank(Investment):
    def dep(self):
        for i in range(self.term_investment * 12):
            self.sum_investment += ((self.sum_investment * 0.1) / 12)
        print(f"Cумма на счету пользователя {self.sum_investment}")


result = Bank(1200, 5, 10)
result.dep()
