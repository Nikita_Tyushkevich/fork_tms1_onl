def reshape(strings: list, row: int, n_2: int):
    matrix = []
    variable = 0
    matrix.append(strings[0:n_2])
    while variable < row - 1:
        matrix.append(strings[n_2 * (variable + 1):n_2 * (variable + 2)])
        variable += 1
    print(matrix)


reshape([1, 2, 3, 4, 5, 6], 2, 3)
reshape([1, 2, 3, 4, 5, 6, 7, 8], 4, 2)
