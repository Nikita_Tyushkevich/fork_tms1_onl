from selenium import webdriver
from time import sleep
browser = webdriver.Chrome("./chromedriver")
browser.implicitly_wait(5)
browser.get("https://ultimateqa.com/filling-out-forms/")

try:
    message_field = browser.find_element_by_xpath(
        "//textarea[@name='et_pb_contact_message_1']")
    message_field.send_keys("Welcome to us")
    button_submit = browser.find_element_by_css_selector(
        ".et_contact_bottom_container :nth-child(2)[type='submit']")
    button_submit.click()
    sleep(2)
    checking_error = browser.find_element_by_css_selector(
        ".et-pb-contact-message >p")
    assert checking_error.text == "Please, fill in the following fields:"
finally:
    browser.quit()
