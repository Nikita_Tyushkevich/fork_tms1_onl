from selenium.webdriver.common.by import By


class LoginPageLocators:

    LOCATOR_EMAIL_FIELD = (By.ID, "email")
    LOCATOR_PASSWORD_FIELD = (By.ID, "passwd")
    LOCATOR_SUBMIT_BUTTON = (By.ID, "SubmitLogin")
    LOCATOR_CREATE_ACCOUNT_TEXT = (By.CLASS_NAME, "page-subheading")
