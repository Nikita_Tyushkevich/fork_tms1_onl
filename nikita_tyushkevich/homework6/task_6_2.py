# Task 6.2 Letters count

def letters_count(str_input):
    """This function counts characters in string"""
    str_input_append = str_input + " "
    count_let = 0
    a = str_input_append[0]
    list1 = []
    for i in str_input_append:
        if i != a:
            if count_let == 1:
                list1.append(a)
            else:
                list1.append(a + str(count_let))
            count_let = 0
            a = i
        count_let += 1
    return "".join(list1)


print(letters_count('cccbba'))
print(letters_count('abeehhhhhccced'))
print(letters_count('aaabbceedd'))
print(letters_count('abcde' + ' '))
print(letters_count('aaabbdefffff'))
