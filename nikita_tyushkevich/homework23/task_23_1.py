# Find button

from selenium import webdriver
from selenium.webdriver.common.by import By

browser = webdriver.Chrome()
browser.implicitly_wait(5)
browser.get("https://ultimateqa.com/complicated-page/")


def test_find_button():
    # By class name
    button = browser.find_element(By.CLASS_NAME, "et_pb_button.et_pb_button_4"
                                                 ".et_pb_bg_layout_light")
    button.click()
    # By CSS selector
    button_2 = browser.find_element(By.CSS_SELECTOR, '[class="et_pb_button '
                                                     'et_pb_button_4 et_pb_'
                                                     'bg_layout_light"]')
    button_2.click()
    # By XPATH
    button_3 = browser.find_element_by_xpath('//a[@class="et_pb_button '
                                             'et_pb_button_4 et_pb_bg_'
                                             'layout_light"]')
    button_3.click()
    browser.quit()
