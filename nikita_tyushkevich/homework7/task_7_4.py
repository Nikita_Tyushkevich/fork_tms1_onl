# Caesar cipher

alphabet_eng = 'abcdefghijklmnopqrstuvwxyz'
alpha_rus = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя'


def cesar(code, num: int, selected_operation: int):
    """This function could encode or decode letter strings,
        regarding selection"""
    # Remove spaces from beginning and end of the string
    strip_code = code.strip()
    encode = []
    decode = []
    if len(code) == 0:
        return "String is empty"
    # Defining of the alphabet
    for letter in code:
        if letter[0] in alphabet_eng or letter[0].lower() in alphabet_eng:
            alpha_s = alphabet_eng
            alph_l = len(alphabet_eng)
        else:
            alpha_s = alpha_rus
            alph_l = len(alpha_rus)
        # Encoding operation is selected
        if selected_operation == 1:
            for i in strip_code:
                # Check not alphabet characters and list append
                if not i.isalpha():
                    encode.append(i)
                # Check for upper cases letters and list append
                elif i == i.upper():
                    i = i.lower()
                    low_i = alpha_s[(alpha_s.index(i) + num) % alph_l]
                    encode.append(low_i.upper())
                # List append with lower cases letters
                else:
                    encode.append(alpha_s[(alpha_s.index(i) + num) % alph_l])
            return ''.join(encode)
        # Decoding operation is selected
        else:
            for i in strip_code:
                if not i.isalpha():
                    decode.append(i)
                elif i == i.upper():
                    i = i.lower()
                    lower_i = alpha_s[(alpha_s.index(i) - num) % alph_l]
                    decode.append(lower_i.upper())
                else:
                    decode.append(alpha_s[(alpha_s.index(i) - num) % alph_l])
            return ''.join(decode)


print(cesar('Hello world!', 5,
            int(input("Encode = 1\n"
                      "Decode = 2\n"
                      "Select needed operation: "))))
