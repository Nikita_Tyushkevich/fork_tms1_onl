# Быки и коровы
from collections import Counter

answer1 = 8465
answer2 = str(answer1)
correct_digits_num = 0
correct_places = 0
players_try = input("Enter your number. 4 digits, which not repeat: ")
players_input = 0
# Check 4 digits in input
if len(players_try) != 4:
    players_try = input("Your number should have 4 unique digits: ")
# Check that values are unique
while players_input != 1:
    for p in Counter(players_try).values():
        if p == 1:
            players_input = 1
        else:
            players_try = input("Digits in your number not unique. "
                                "Please enter another 4 unique "
                                "digits number: ")
            players_input = 0
            break
# Search for intersecting values
while answer2 != players_try:
    for i in players_try:
        if i == answer2[0] or i == answer2[1] or i == answer2[2] \
                or i == answer2[3]:
            correct_digits_num += 1
            # Search for digits at the same places in numbers
    for b in range(len(players_try)):
        if b == 0 and answer2[0] == players_try[0] \
                or b == 1 and answer2[1] == players_try[1] \
                or b == 2 and answer2[2] == players_try[2] \
                or b == 3 and answer2[3] == players_try[3]:
            correct_places += 1
            correct_digits_num -= 1
    # Result for Cows and Bulls
    print(f"Cows: {correct_digits_num}, Bulls: {correct_places}."
          f"Try again!")
    correct_digits_num = 0
    correct_places = 0
    players_input = 0
    players_try = input("Another try: ")
    # Check 4 digits in input
    if len(players_try) != 4:
        players_try = input("Your number should have 4 unique digits: ")
    # Check that values are unique. Break is needed for cases when not only
    # one digit repeats
    while players_input != 1:
        for p in Counter(players_try).values():
            if p == 1:
                players_input = 1
            else:
                players_try = input("Digits in your number not unique. "
                                    "Please enter another 4 unique "
                                    "digits number: ")
                players_input = 0
                break
# Number was guessed
if answer2 == players_try:
    print("You're right!")
