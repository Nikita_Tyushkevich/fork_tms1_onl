from selenium import webdriver


def test_exercise():
    link = 'http://thedemosite.co.uk/login.php'
    driver = webdriver.Chrome('./chromedriver.exe')
    driver.implicitly_wait(5)
    driver.get(link)
    field_1 = driver.find_element_by_name('username')
    field_1.send_keys("fdgghgfhgf")
    field_2 = driver.find_element_by_name('password')
    field_2.send_keys("dfgdfg")
    button = driver.find_element_by_name('FormsButton2')
    button.click()
    text = driver.find_element_by_xpath('//b[contains(., "**Successful'
                                        ' Login**")]')
    assert text.text == "**Successful Login**"
    driver.quit()
