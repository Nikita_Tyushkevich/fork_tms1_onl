"""Заполните форму обратной связи. Проверьте наличие сообщений."""

import pytest


class TestForm:

    @pytest.fixture
    def form_data(self, browser):
        browser.get("https://ultimateqa.com/filling-out-forms/")

        self.name = browser.find_element_by_id("et_pb_contact_name_0")
        self.msg = browser.find_element_by_id("et_pb_contact_message_0")
        xp = "//div[@id='et_pb_contact_form_0']//button"
        self.button = browser.find_element_by_xpath(xp)
        self.input_1 = "Casey"
        self.input_2 = "Hi!"

    def test_form_success(self, browser, form_data):
        self.name.send_keys(self.input_1)
        self.msg.send_keys(self.input_2)
        self.button.click()

        xp_test = "//div[@id='et_pb_contact_form_0']/div/p"
        test = browser.find_element_by_xpath(xp_test)

        assert test.text == "Thanks for contacting us"

    def test_name_only(self, browser, form_data):
        self.name.send_keys(self.input_1)
        self.button.click()

        xp_test = "//div[@id='et_pb_contact_form_0']//li"
        test = browser.find_element_by_xpath(xp_test)

        assert test.text == "Message"

    def test_message_only(self, browser, form_data):
        self.msg.send_keys(self.input_2)
        self.button.click()

        xp_test = "//div[@id='et_pb_contact_form_0']//li"
        test = browser.find_element_by_xpath(xp_test)

        assert test.text == "Name"
