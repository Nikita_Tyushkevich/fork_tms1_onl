def word(chr):
    chr_new = chr + " "
    ctr, chr_ind, lst = 0, chr_new[0], []
    for i in chr_new:
        if chr_ind != i:
            lst.append(chr_ind + str(ctr).replace("1", ""))
            ctr, chr_ind = 0, i
        ctr += 1
    chr_final = "".join(lst)
    return chr_final
