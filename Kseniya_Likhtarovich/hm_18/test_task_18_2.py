import unittest
from main_task_18_2 import game


class TestGame(unittest.TestCase):

    def setUp(self):
        self.error_message = "Нужно ввести 4 НЕПОВТОРЯЮЩИЕСЯ ЦИФРЫ!"
        self.success_message = "Вы победитель по жизни! Быки: 4, Коровы: 0"

    def test_game_1(self):
        self.assertEqual(game("1235", "9876"), "Быки: 0, Коровы: 0.")

    def test_game_2(self):
        self.assertEqual(game("2468", "8642"), "Быки: 0, Коровы: 4.")

    def test_game_3(self):
        self.assertEqual(game("4967", "9467"), "Быки: 2, Коровы: 2.")

    def test_game_4(self):
        self.assertEqual(game("3257", "0235"), "Быки: 1, Коровы: 2.")

    def test_game_5(self):
        self.assertEqual(game("2348", "2148"), "Быки: 3, Коровы: 0.")

    def test_game_6(self):
        self.assertEqual(game("6285", "6285"), self.success_message)

    @unittest.expectedFailure
    def test_game_7(self):
        self.assertEqual(game("628", "6254"), self.error_message)

    @unittest.expectedFailure
    def test_game_8(self):
        self.assertEqual(game("62835", "6254"), self.error_message)

    @unittest.expectedFailure
    def test_game_9(self):
        self.assertEqual(game("6623", "2468"), self.error_message)

    @unittest.expectedFailure
    def test_game_10(self):
        self.assertEqual(game("63 95", "6395"), self.error_message)

    @unittest.expectedFailure
    def test_game_11(self):
        self.assertEqual(game("91a3", "2498"), self.error_message)

    @unittest.expectedFailure
    def test_game_12(self):
        self.assertEqual(game("23&5", "1287"), self.error_message)

    @unittest.expectedFailure
    def test_game_13(self):
        self.assertEqual(game("", "8392"), self.error_message)


if __name__ == "__main__":
    unittest.main()
