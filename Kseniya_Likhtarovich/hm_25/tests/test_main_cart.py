from Kseniya_Likhtarovich.hm_25.pages.main_page import MainPage
from Kseniya_Likhtarovich.hm_25.pages.cart_page import CartPage


def test_cart(browser):
    main_page = MainPage(browser)
    main_page.open_main_page()
    main_page.open_cart_page()

    cart_page = CartPage(browser)
    cart_page.should_be_cart_page()
    cart_page.cart_is_empty()
