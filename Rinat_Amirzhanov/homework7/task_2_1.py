def list_numbers(num):
    for i in num:
        if i > 0:
            yield i


numbers = [34.6, -203.4, 44.9, 68.3, -12.2, 44.6, 12.7]
formatted_numbers = [x for x in list_numbers(numbers)]
print(formatted_numbers)
