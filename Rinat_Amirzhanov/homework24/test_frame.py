def test_Frame(browser):
    browser.get('http://the-internet.herokuapp.com/frames')
    browser.find_element_by_xpath(
        '//*[@id="content"]/div/ul/li[2]/a').click()
    browser.switch_to.frame(browser.find_element_by_xpath(
        '//*[@id="mce_0_ifr"]'))
    res_text = browser.find_element_by_xpath(
        '//*[@id="tinymce"]/p').text
    assert res_text == "Your content goes here."
