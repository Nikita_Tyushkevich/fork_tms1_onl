import wikipediaapi


def test_wiki_api():
    word = "Programming language"
    wiki_page = wikipediaapi.Wikipedia('en')
    article_page = wiki_page.page(word)
    assert article_page.exists()
    assert article_page.title == word
