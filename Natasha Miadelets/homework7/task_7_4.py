ru_alf = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя"
RU_ALF = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
eng_alf = "abcdefghijklmnopqrstuvwxyz"
ENG_ALF = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
digits = "0123456789"
simbols = ".,;:?!()-+=/*^%&#$@№"
# text = "Bgtq Бгнрм 41!"
key = 2

# encode() принимает строку с любыми буквами, цифрами и символами.
# Сохраняет регистр и символы без изменения и кодирует на заданный key


def encode():
    text = input("Введите текст:")
    text_new = ""
    for i in text:
        plase_eng_alf = eng_alf.find(i)
        plase_ENG_ALF = ENG_ALF.find(i)
        plase_ru_alf = ru_alf.find(i)
        plase_RU_ALF = RU_ALF.find(i)
        plase_digits = digits.find(i)
        if i in eng_alf:
            new_plase = plase_eng_alf + key
            if new_plase <= len(eng_alf) - 1:
                text_new += eng_alf[new_plase]
            else:
                text_new += eng_alf[new_plase - len(eng_alf)]
        elif i in ENG_ALF:
            new_plase = plase_ENG_ALF + key
            if new_plase <= len(ENG_ALF) - 1:
                text_new += ENG_ALF[new_plase]
            else:
                text_new += ENG_ALF[new_plase - len(ENG_ALF)]
        elif i in ru_alf:
            new_plase = plase_ru_alf + key
            if new_plase <= len(ru_alf) - 1:
                text_new += ru_alf[new_plase]
            else:
                text_new += ru_alf[new_plase - len(ru_alf)]
        elif i in RU_ALF:
            new_plase = plase_RU_ALF + key
            if new_plase <= len(RU_ALF) - 1:
                text_new += RU_ALF[new_plase]
            else:
                text_new += RU_ALF[new_plase - len(RU_ALF)]
        elif i in simbols or i == " ":
            text_new += i
        elif i in digits:
            new_plase = plase_digits + key
            if new_plase <= len(digits) - 1:
                text_new += digits[new_plase]
            else:
                text_new += digits[new_plase - len(digits)]
    print(text_new)


def decode():
    text = input("Введите текст:")
    text_new = ""
    for i in text:
        plase_eng_alf = eng_alf.find(i)
        plase_ENG_ALF = ENG_ALF.find(i)
        plase_ru_alf = ru_alf.find(i)
        plase_RU_ALF = RU_ALF.find(i)
        plase_digits = digits.find(i)
        if i in eng_alf:
            new_plase = plase_eng_alf - key
            if new_plase <= len(eng_alf) - 1:
                text_new += eng_alf[new_plase]
            else:
                text_new += eng_alf[new_plase - len(eng_alf)]
        elif i in ENG_ALF:
            new_plase = plase_ENG_ALF - key
            if new_plase <= len(ENG_ALF) - 1:
                text_new += ENG_ALF[new_plase]
            else:
                text_new += ENG_ALF[new_plase - len(ENG_ALF)]
        elif i in ru_alf:
            new_plase = plase_ru_alf - key
            if new_plase <= len(ru_alf) - 1:
                text_new += ru_alf[new_plase]
            else:
                text_new += ru_alf[new_plase - len(ru_alf)]
        elif i in RU_ALF:
            new_plase = plase_RU_ALF - key
            if new_plase <= len(RU_ALF) - 1:
                text_new += RU_ALF[new_plase]
            else:
                text_new += RU_ALF[new_plase - len(RU_ALF)]
        elif i in simbols or i == " ":
            text_new += i
        elif i in digits:
            new_plase = plase_digits - key
            if new_plase <= len(digits) - 1:
                text_new += digits[new_plase]
            else:
                text_new += digits[new_plase - len(digits)]
    print(text_new)


def select_code():
    code = int(input("Выбери - 1. Закодировать 2. Расшифровать:"))
    if code == 1:
        encode()
    else:
        decode()


select_code()
