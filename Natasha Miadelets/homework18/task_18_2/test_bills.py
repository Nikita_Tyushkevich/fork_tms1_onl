from unittest import TestCase
from unittest import expectedFailure
from main_bills import Game


class TestGame(TestCase):

    def setUp(self):
        self.number_1 = "3456"
        self.number_2 = "3489"
        self.number_3 = "3468"
        self.number_4 = "5364"
        self.number_5 = "1298"
        self.number_6 = "9346"
        self.answer_1 = "Вы выйграли!"
        self.answer_2 = '0 коровы,', '2 быка'
        self.answer_3 = '1 коровы,', '2 быка'
        self.answer_4 = '4 коровы,', '0 быка'
        self.answer_5 = '0 коровы,', '0 быка'
        self.answer_6 = '2 коровы,', '1 быка'
        self.game = Game()

    def test_bulls_4(self):
        self.assertEqual(self.game.bulls_and_cows(self.number_1),
                         self.answer_1)

    def test_cows_0_bulls_2(self):
        self.assertEqual(self.game.bulls_and_cows(self.number_2),
                         self.answer_2)

    def test_cows_1_bulls_2(self):
        self.assertEqual(self.game.bulls_and_cows(self.number_3),
                         self.answer_3)

    def test_cows_4(self):
        self.assertEqual(self.game.bulls_and_cows(self.number_4),
                         self.answer_4)

    def test_cows_0_bulls_0(self):
        self.assertEqual(self.game.bulls_and_cows(self.number_5),
                         self.answer_5)

    def test_cows_2_bulls_1(self):
        self.assertEqual(self.game.bulls_and_cows(self.number_6),
                         self.answer_6)

    @expectedFailure
    def test_invalid_answer_1(self):
        self.assertEqual(self.game.bulls_and_cows(self.number_4),
                         self.answer_1)

    @expectedFailure
    def test_invalid_answer_2(self):
        self.assertEqual(self.game.bulls_and_cows(self.number_6),
                         self.answer_3)
